# translation of debian-cdd.po to தமிழ்
# ஸ்ரீராமதாஸ் <shriramadhas@gmail.com>, 2007.
msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Last-Translator: ஸ்ரீராமதாஸ் <shriramadhas@gmail.com>\n"
"PO-Revision-Date: 2007-11-06 18:20+0530\n"
"Project-Id-Version: debian-cdd\n"
"Language-Team: தமிழ் <ubuntu-l10n-tam@lists.ubuntu.com>\n"
"X-Generator: KBabel 1.11.4\n"
"MIME-Version: 1.0\n"

#: ../../english/template/debian/debian-cdd.wml:28
#: ../../english/template/debian/debian-cdd.wml:68
msgid "Debian-Med"
msgstr "டெபியன்-மருத்துவம்"

#: ../../english/template/debian/debian-cdd.wml:31
#: ../../english/template/debian/debian-cdd.wml:72
msgid "Debian-Jr"
msgstr "டெபியன்-குட்டி"

#: ../../english/template/debian/debian-cdd.wml:34
#: ../../english/template/debian/debian-cdd.wml:76
msgid "Debian-Lex"
msgstr "டெபியன்-அகராதி"

#: ../../english/template/debian/debian-cdd.wml:36
msgid "The list below includes various software projects which are of some interest to the <get-var cdd-name /> Project. Currently, only a few of them are available as Debian packages. It is our goal, however, to include all software in <get-var cdd-name /> which can sensibly add to a high quality Custom Debian Distribution."
msgstr "<get-var cdd-name /> திட்டத்துக்கு சிறிய அளவிலேனும் தொடர்புடையப் பலவேறுத் திட்டங்கள் கீழே பட்டியலிடப் படுகின்றன. அவற்றுள் சில மாத்திரமே டெபியன் பொதியாகக் கிடைக்கப் பெறுகின்றன. ஆயினும்,  உயர்தரம் வாய்ந்த தரமான டெபியன் வழங்களுக்குள் சேர்க்க வல்ல அனைத்து மென்பொருட்களையுமே <get-var cdd-name /> க்குள் அடக்குவது எங்கள் இலக்கு."

#: ../../english/template/debian/debian-cdd.wml:41
msgid "For a better overview of the project's availability as a Debian package, each head row has a color code according to this scheme:"
msgstr "திட்டமொன்று டெபியன் பொதியாக் கிடைக்கிறதா என்பது குறித்து தெளிவாக உணர்த்தும் பொருட்டு, இம்முறையின்படி ஒவ்வொரு தலை வரிசைக்கும் ஒரு நிறம் கொண்டு விளங்குகின்றன: "

#: ../../english/template/debian/debian-cdd.wml:47
msgid "Green: The project is <a href=\"<get-var url />\">available as an official Debian package</a>"
msgstr "பச்சை: <a href=\"<get-var url />\"> திட்டம் அதிகாரப் பூர்வ டெபியன் பொதியாகமாகக் கிடைக்கப் பெறுகிறது</a>"

#: ../../english/template/debian/debian-cdd.wml:54
msgid "Yellow: The project is <a href=\"<get-var url />\">available as an inofficial Debian package</a>"
msgstr "மஞ்சள்: <a href=\"<get-var url />\">திட்டம் அதிகாரப் பூர்வமற்ற டெபியன் பொதியாகமாகக் கிடைக்கப் பெறுகிறது</a>"

#: ../../english/template/debian/debian-cdd.wml:61
msgid "Red: The project is <a href=\"<get-var url />\">not (yet) available as a Debian package</a>"
msgstr "சிகப்பு: <a href=\"<get-var url />\">டெபியன் பொதியாக இன்னும் கிடைக்கப் பெறவில்லை</a>"

#: ../../english/template/debian/debian-cdd.wml:79
msgid "If you discover a project which looks like a good candidate for <get-var cdd-name /> to you, or if you have prepared an inofficial Debian package, please do not hesitate to send a description of that project to the <a href=\"<get-var cdd-list-url />\"><get-var cdd-name /> mailing list</a>."
msgstr "<get-var cdd-name />க்கு உகந்ததாக தாங்கள் ஒருத் திட்டத்தை இனங்கண்டாலோ, அல்லது அதிகாரப் பூர்வமற்ற பொதியொன்றினை உருவாக்கியிருந்தாலோ, அத்திட்டம் குறித்த விவரங்களை <a href=\"<get-var cdd-list-url />\"><get-var cdd-name /> மடலாடற் குழுக்களுக்கு அனுப்ப மறக்க வேண்டாம்</a>."

#: ../../english/template/debian/debian-cdd.wml:84
msgid "Official Debian packages"
msgstr "அதிகாரப் பூர்வ டெபியன் பொதிகள்"

#: ../../english/template/debian/debian-cdd.wml:88
msgid "Inofficial Debian packages"
msgstr "அதிகாரப் பூர்வமற்ற டெபியன் பொதிகள்"

#: ../../english/template/debian/debian-cdd.wml:92
msgid "Debian packages not available"
msgstr "டெபியன் பொதிகள் கிடைக்கப் பெறவில்லை"

#: ../../english/template/debian/debian-cdd.wml:96
msgid "There are no projects which fall into this category."
msgstr "எந்தத் திட்டமும் இவ்வகையில் அடங்கவில்லை."

#: ../../english/template/debian/debian-cdd.wml:100
msgid "No homepage known"
msgstr "முததற் பக்கம் அறியப் படவில்லை"

#: ../../english/template/debian/debian-cdd.wml:104
msgid "License:"
msgstr "உரிமம்:"

#: ../../english/template/debian/debian-cdd.wml:108
msgid "Free"
msgstr "கட்டற்ற"

#: ../../english/template/debian/debian-cdd.wml:112
msgid "Non-Free"
msgstr "கட்டுடைய"

#: ../../english/template/debian/debian-cdd.wml:116
msgid "Unknown"
msgstr "அறியப்படாத"

#: ../../english/template/debian/debian-cdd.wml:120
msgid "Official Debian package"
msgstr "அதிகாபூர்வ டெபியன் பொதி"

#: ../../english/template/debian/debian-cdd.wml:124
msgid "Inofficial Debian package"
msgstr "அதிகாரப் பூர்வமற்ற டெபியன் பொதி"

#: ../../english/template/debian/debian-cdd.wml:128
msgid "Debian package not available"
msgstr "டெபியன் பொதி கிடைக்கவில்லை"

