<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there was a cross-site scripting (XSS) vulnerability 
in phpldapadmin, a web-based interface for administering LDAP servers.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.2.2-5.2+deb8u1.
<p><b>Note</b>: the package changelog mistakenly refers to the non-existent
<a href="https://security-tracker.debian.org/tracker/CVE-2016-11107">CVE-2016-11107</a> 
identifier. The proper identifier to refer to this issue
is <a href="https://security-tracker.debian.org/tracker/CVE-2017-11107">CVE-2017-11107</a>.</p>

<p>We recommend that you upgrade your phpldapadmin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1561.data"
# $Id: $
