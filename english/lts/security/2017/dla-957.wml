<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3136">CVE-2017-3136</a>

     <p>Oleg Gorokhov of Yandex discovered that BIND does not properly
     handle certain queries when using DNS64 with the "break-dnssec yes;"
     option, allowing a remote attacker to cause a denial-of-service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3137">CVE-2017-3137</a>

     <p>It was discovered that BIND makes incorrect assumptions about the
     ordering of records in the answer section of a response containing
     CNAME or DNAME resource records, leading to situations where BIND
     exits with an assertion failure. An attacker can take advantage of
     this condition to cause a denial-of-service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3138">CVE-2017-3138</a>

     <p>Mike Lalumiere of Dyn, Inc. discovered that BIND can exit with a
     REQUIRE assertion failure if it receives a null command string on
     its control channel. Note that the fix applied in Debian is only
     applied as a hardening measure. Details about the issue can be found
     at <a href="https://kb.isc.org/article/AA-01471">https://kb.isc.org/article/AA-01471</a>.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:9.8.4.dfsg.P1-6+nmu2+deb7u16.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-957.data"
# $Id: $
