<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues were discovered in mysql-connector-java that allow
attackers to execute arbitrary code, insert or delete access to some
of MySQL Connectors accessible data as well as unauthorized read
access to a subset of the data.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.1.42-1~deb7u1.</p>

<p>We recommend that you upgrade your mysql-connector-java packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-945.data"
# $Id: $
