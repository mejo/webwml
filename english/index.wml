#use wml::debian::mainpage title="The Universal Operating System" GEN_TIME="yes"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"



<span class="download"><a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">Download Debian <current_release_short><em>(64-bit PC Network installer)</em></a> </span>


<div id="splash">
	<h1>Debian</h1>
</div>

<div id="intro">
<p>Debian is a <a href="intro/free">free</a> operating system (OS) for your computer.
An operating system is the set of basic programs and utilities that make
your computer run.
</p>

<p>Debian provides more than a pure OS: it comes with over
<packages_in_stable> <a href="distrib/packages">packages</a>, precompiled software bundled
up in a nice format for easy installation on your machine. <a href="intro/about">Read more...</a></p>
</div>

<hometoc/>

<p class="infobar">The <a href="releases/stable/">latest stable release of Debian</a> is
<current_release_short>. The last update to this release was made on
<current_release_date>. Read more about <a href="releases/">available
versions of Debian</a>.</p>
<h2>Getting Started</h2>
<p>Please use the navigation bar at the top of this page to access more content.</p>
<p>In addition, users that speak languages other than English may check the 
<a href="international/">international</a> section, and, people who use systems other
than Intel x86 should check the <a href="ports/">ports</a> section.</p>

<hr />
<a class="rss_logo" href="News/news">RSS</a>
<h2>News</h2>

<p><:= get_recent_list('News/$(CUR_YEAR)', '6', '$(ENGLISHDIR)', '', '\d+\w*' ) :></p>

<p>For older news items see the <a href="$(HOME)/News/">News Page</a>.
If you would like to receive mail whenever new Debian news comes out, subscribe to the
<a href="MailingLists/debian-announce">debian-announce mailing list</a>.</p>

<hr />
<a class="rss_logo" href="security/dsa">RSS</a>
<h2>Security Advisories</h2>

<p><:= get_recent_list ('security/2w', '10', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)' ) :></p>

<p>For older security advisories see the <A href="$(HOME)/security/">
Security Page</a>.
If you would like to receive security advisories as soon as they're announced, subscribe to the
<a href="https://lists.debian.org/debian-security-announce/">debian-security-announce
mailing list</a>.</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian News" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Debian Project News" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="security/dsa-long">
:#rss#}
