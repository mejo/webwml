# translation of organization.po to Arabic
# Isam Bayazidi <isam@bayazidi.net>, 2004, 2005.
# Ossama M. Khayat <okhayat@yahoo.com>, 2005.
# Mohamed Amine <med@mailoo.org>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: organization.ar\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-05-08 17:17+0000\n"
"PO-Revision-Date: 2013-05-08 17:23+0000\n"
"Last-Translator: Mohamed Amine <med@mailoo.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Virtaal 0.7.1\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "الحالي"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "عضو"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "مدير"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr ""

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr ""

#: ../../english/intro/organization.data:39
#, fuzzy
#| msgid "chairman"
msgid "chair"
msgstr "رئيس المجلس"

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr ""

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "أمين عام"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:66
msgid "Officers"
msgstr "أعضاء"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:90
msgid "Distribution"
msgstr "توزيعة"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:232
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:235
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:58
#: ../../english/intro/organization.data:239
#, fuzzy
#| msgid "Publicity"
msgid "Publicity team"
msgstr "الدعاية والإعلان"

#: ../../english/intro/organization.data:60
#: ../../english/intro/organization.data:304
msgid "Support and Infrastructure"
msgstr "دعم فني وبنية تحتية"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:62
msgid "Debian Pure Blends"
msgstr ""

#: ../../english/intro/organization.data:69
msgid "Leader"
msgstr "قائد"

#: ../../english/intro/organization.data:71
msgid "Technical Committee"
msgstr "اللجنة التقنية"

#: ../../english/intro/organization.data:85
msgid "Secretary"
msgstr "أمين عام"

#: ../../english/intro/organization.data:93
msgid "Development Projects"
msgstr "مشاريع التطوير"

#: ../../english/intro/organization.data:94
msgid "FTP Archives"
msgstr "أرشيف FTP "

#: ../../english/intro/organization.data:96
msgid "FTP Masters"
msgstr ""

#: ../../english/intro/organization.data:102
msgid "FTP Assistants"
msgstr "مساعد FTP"

#: ../../english/intro/organization.data:107
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:111
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:113
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:117
msgid "Individual Packages"
msgstr "حزم منفردة"

#: ../../english/intro/organization.data:118
msgid "Release Management"
msgstr "إدارة الإصدارات"

#: ../../english/intro/organization.data:120
msgid "Release Team"
msgstr ""

#: ../../english/intro/organization.data:133
msgid "Quality Assurance"
msgstr "ضمان الجودة"

#: ../../english/intro/organization.data:134
msgid "Installation System Team"
msgstr "فريق نظام التثبيت"

#: ../../english/intro/organization.data:135
msgid "Release Notes"
msgstr "ملاحظات الإصدارة"

#: ../../english/intro/organization.data:137
msgid "CD Images"
msgstr "ملفات قرص مدمج"

#: ../../english/intro/organization.data:139
msgid "Production"
msgstr "الإنتاج"

#: ../../english/intro/organization.data:147
msgid "Testing"
msgstr "اختبار"

#: ../../english/intro/organization.data:149
msgid "Autobuilding infrastructure"
msgstr ""

#: ../../english/intro/organization.data:151
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:159
msgid "Buildd administration"
msgstr ""

#: ../../english/intro/organization.data:178
msgid "Documentation"
msgstr "التوثيق"

#: ../../english/intro/organization.data:183
msgid "Work-Needing and Prospective Packages list"
msgstr "قائمة الحزم التي هي بحاجة إلى مزيد من العمل والمتوقعة"

#: ../../english/intro/organization.data:186
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:187
msgid "Ports"
msgstr "تحويلات"

#: ../../english/intro/organization.data:222
msgid "Special Configurations"
msgstr "إعدادات خاصة"

#: ../../english/intro/organization.data:225
msgid "Laptops"
msgstr "الحواسب المحمولة"

#: ../../english/intro/organization.data:226
msgid "Firewalls"
msgstr "الجدران النارية"

#: ../../english/intro/organization.data:227
msgid "Embedded systems"
msgstr "الأجهزة المدمجة"

#: ../../english/intro/organization.data:242
msgid "Press Contact"
msgstr "جهة الإتصال الصحفي"

#: ../../english/intro/organization.data:244
msgid "Web Pages"
msgstr "صفحات الإنترنت"

#: ../../english/intro/organization.data:254
msgid "Planet Debian"
msgstr ""

#: ../../english/intro/organization.data:259
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:263
msgid "Debian Women Project"
msgstr ""

#: ../../english/intro/organization.data:271
msgid "Anti-harassment"
msgstr ""

#: ../../english/intro/organization.data:276
msgid "Events"
msgstr "أحداث"

#: ../../english/intro/organization.data:282
#, fuzzy
#| msgid "Technical Committee"
msgid "DebConf Committee"
msgstr "اللجنة التقنية"

#: ../../english/intro/organization.data:289
msgid "Partner Program"
msgstr "برامج الشراكة"

#: ../../english/intro/organization.data:294
msgid "Hardware Donations Coordination"
msgstr "تنسيق التبرعات بالعتاد والأجهزة"

#: ../../english/intro/organization.data:307
msgid "User support"
msgstr "دعم المستخدم"

#: ../../english/intro/organization.data:374
msgid "Bug Tracking System"
msgstr "نظام تتبع العلاّت"

#: ../../english/intro/organization.data:379
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr ""

#: ../../english/intro/organization.data:387
msgid "New Members Front Desk"
msgstr ""

#: ../../english/intro/organization.data:393
msgid "Debian Account Managers"
msgstr "مدراء حسابات دبيان"

#: ../../english/intro/organization.data:397
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:398
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "مشرفو مفاتيح التشفير PGP و GPG"

#: ../../english/intro/organization.data:402
msgid "Security Team"
msgstr "الفريق الأمني"

#: ../../english/intro/organization.data:414
msgid "Consultants Page"
msgstr "صفحة المستشارين"

#: ../../english/intro/organization.data:419
msgid "CD Vendors Page"
msgstr "صفحة موزعي الأقراص المدمجة"

#: ../../english/intro/organization.data:422
msgid "Policy"
msgstr "سياسة"

#: ../../english/intro/organization.data:425
msgid "System Administration"
msgstr "إدارة النظام"

#: ../../english/intro/organization.data:426
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"هذا هو العنوان الذي يجب استخدامه عند مواجهة مشاكل اتصال في أحد أجهزة مشروع "
"دبيان، وهذا يشمل المشاكل مع الدخول وكلمة السر، والحصول على حزمة معينة "
"للتثبيت."

#: ../../english/intro/organization.data:435
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"إذا واجهت مشاكل مع العتاد في أحد أجهزة دبيان، يرجى مراجعة <a href=\"https://"
"db.debian.org/machines.cgi\">أجهزة دبيان</a> للحصول على معلومات الإتصال "
"الخاصة بالمدير المسؤول عن الجهاز."

#: ../../english/intro/organization.data:436
msgid "LDAP Developer Directory Administrator"
msgstr "مدير دليل LDAP لمعلومات المطورين"

#: ../../english/intro/organization.data:437
msgid "Mirrors"
msgstr "المرايا"

#: ../../english/intro/organization.data:444
msgid "DNS Maintainer"
msgstr "مشرف ال DNS"

#: ../../english/intro/organization.data:445
msgid "Package Tracking System"
msgstr "نظام تتبع الحزم"

#: ../../english/intro/organization.data:447
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:453
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:456
#, fuzzy
#| msgid "Alioth administrators"
msgid "Salsa administrators"
msgstr "مدراء اولاين"

#: ../../english/intro/organization.data:467
msgid "Debian for children from 1 to 99"
msgstr "ديبيان للأطفال من سنة الى 99 سنوات"

#: ../../english/intro/organization.data:470
msgid "Debian for medical practice and research"
msgstr "دبيان للأغراض الطبية والبحث الطبي"

#: ../../english/intro/organization.data:473
msgid "Debian for education"
msgstr "دبيان للتعليم"

#: ../../english/intro/organization.data:478
msgid "Debian in legal offices"
msgstr "دبيان في المكاتب القانونية"

#: ../../english/intro/organization.data:482
msgid "Debian for people with disabilities"
msgstr "دبيان لدوي الاحتياجات الخاصة"

#: ../../english/intro/organization.data:486
msgid "Debian for science and related research"
msgstr ""

#: ../../english/intro/organization.data:489
#, fuzzy
#| msgid "Debian for education"
msgid "Debian for astronomy"
msgstr "دبيان للتعليم"

#~ msgid "Publicity"
#~ msgstr "الدعاية والإعلان"

#~ msgid "Security Audit Project"
#~ msgstr "مشروع التدقيق الأمني"

#~ msgid "Alioth administrators"
#~ msgstr "مدراء اولاين"
