#use wml::debian::template title="Debian sul Desktop"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="fa8e80162815f92b0d792ca4837df20cdc61c896" maintainer="Luca Monducci"

<h2>Il Sistema Operativo Universale come vostro Desktop</h2>

<p>
  Il sottoprogetto Debian Desktop è composto da un gruppo di volontari
  che vogliono creare il miglior sistema operativo possibile
  per i computer di casa e aziendali. Il nostro motto è <q>Software
  che funziona</q>. In breve, il nostro obiettivo è introdurre Debian,
  GNU e Linux nel mondo tradizionale.
  <img style="float: right;" src="debian-desktop.png" alt="Debian Desktop"/>
</p>
<h3>Le nostre opinioni</h3>
<ul>
  <li>
    Dato che esistono già molti <a
    href="https://wiki.debian.org/DesktopEnvironment">Ambienti Desktop</a>,
    vogliamo sostenerne l'uso, ed essere certi del loro buon funzionamento
    in Debian.
  </li>
  <li>
    Sappiamo che esistono solo due importanti gruppi di utenti:
    il principiante e l'esperto. Faremo tutto il possibile per rendere
    le cose facili per il principiante, permettendo all'esperto
    di modificare le cose a piacimento.
  </li>
  <li>
    Cercheremo di garantire che i programmi siano configurati per i
    tipici usi desktop. Ad esempio, il normale account utente
    creato nella fase di installazione dovrebbe avere
    l'autorizzazione per visionare video, ascoltare audio, stampare e
    amministrare il sistema attraverso sudo.
  </li>
  <li>
    Cercheremo di garantire che le domande (che dovrebbero essere il minor
    numero possibile) poste agli utenti siano comprensibili anche
    a chi ha minima conoscenza del calcolatore. Alcuni pacchetti Debian
    vengono presentati oggi all'utente con difficili dettagli tecnici.
    Si deve evitare di porre all'utente le domande tecniche di debconf.
    Per i meno esperti queste creano confuzione e paura, per i gli esperti
    potrebbero risultare noiose e superflue. Un neofita potrebbe persino
    non sapere a cosa fanno riferimento. Un utente esperto può comunque
    configurare al meglio l'ambiente desktop al termine dell'installazione.
    La priorità di questo tipo di domande di Debconf dovrebbe per lo meno
    essere diminuita.
  </li>
  <li>
    E ci divertiremo facendo tutto ciò!
  </li>
</ul>
<h3>Come potete aiutarci</h3>
<p>
  Le parti più importanti di un sottoprogetto Debian non sono costituite
  da mailing list, pagine web, o spazio per archivi di pacchetti.
  La parte più importante è <em>la gente motivata</em> che renda realizzabili
  le cose. Non avete bisogno di essere sviluppatori ufficiali per
  iniziare e produrre pacchetti e patch. Il gruppo centrale Debian Desktop
  garantisce che il vostro lavoro sarà integrato.
  Ecco alcune cose che potete fare:
</p>
<ul>
  <li>
    Testare il nostro task <q>Ambiente desktop predefinito</q> (oppure
    il task kde-desktop) installando una delle
    <a href="$(DEVEL)/debian-installer/">immagini della prossima release</a>
    e inviando le proprie osservazioni alla lista di messaggi
    <a href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.
  </li>
  <li>
    Lavorare su <a href="$(DEVEL)/debian-installer/">debian-installer</a>.
    L'interfaccia GTK+ ha bisogno di voi.
  </li>
  <li>
	Contribuire nei gruppi <a
	href="https://wiki.debian.org/Teams/DebianGnome">Debian GNOME</a>,
	<a href="https://qt-kde-team.pages.debian.net/">Debian Qt e KDE</a>
	o <a href="https://salsa.debian.org/xfce-team/">Debian Xfce</a>.
	Puoi contribuire alla preparazione del pacchetto, cercando dei bug,
	lavorando alla documentazione, facendo dei test o in altri modi.
  </li>
  <li>
    Insegnare agli utenti come installare e utilizzare i nuovi task di
    Debian desktop (desktop, gnome-desktop e kde-desktop).
  </li>
  <li>
    Lavorare per abbassare le priorità delle domande di
    <a href="https://packages.debian.org/debconf">debconf</a> dai pacchetti, o
    rimuovere quelle non necessarie, e rendere quelle indispensabili facili da
    capire.
   </li>
  <li>
    Aiutare il progetto
    <a href="https://wiki.debian.org/DebianDesktop/Artwork">Debian Desktop
    Artwork</a>.
  </li>
</ul>
<h3>Wiki</h3>
<p>
  Nel nostro wiki sono presenti alcuni articoli, il punto di partenza è
  <a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a>. Una
  parte degli articoli nel wiki è vecchia.
</p>
<h3>Liste di messaggi</h3>
<p>
  Le discussioni di questo sottoprogetto avvengono nella lista di messaggi
  <a href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.
</p>
<h3>Canale IRC</h3>
<p>
  Invitiamo chiunque (sviluppatore Debian o no) sia interessato a
  Debian Desktop a unirsi a #debian-desktop su
  <a href="http://oftc.net/">OFTC IRC</a> (irc.debian.org).
</p>
<h3>Chi è coinvolto?</h3>
<p>
  Chiunque voglia esserlo è il benvenuto! Attualmente sono indirettamente
  conivolti tutti i partecipanti ai gruppi pkg-gnome, pkg-kde e pkg-xfce.
  Gli iscritti alla lista di messaggi debian-desktop sono dei partecipanti
  attivi. Anche i gruppi debian-installer e tasksel sono molto importanti
  per il nostro scopo.
</p>
<p>
  Questa pagina web è manutenuta da
  <a href="https://people.debian.org/~stratus/">Gustavo Franco</a>. In passato
  è stata manutenuta da <a href="https://people.debian.org/~madkiss/">Martin
  Loschwitz</a> e da <a href="https://people.debian.org/~walters/">Colin
  Walters</a>.
</p>
