#use wml::debian::template title="Debian GNU/kFreeBSD"
#use wml::debian::translation-check translation="3947d6bbe0a054d91d469518abb2d4b60be9f691"



#use wml::debian::toc

<toc-display>

<p>Debian GNU/kFreeBSD är en portering som består av
<a href="https://www.gnu.org/">GNU:s användarland</a> med
<a href="https://www.gnu.org/software/libc/">GNU:s C-bibliotek</a> ovanpå
<a href="https://www.freebsd.org/">FreeBSD</a>:s kärna, tillsammans med den
vanliga <a href="https://packages.debian.org/">uppsättningen Debianpaket</a>.</p>

<div class="important">
<p>Debian GNU/kFreeBSD är inte en arkitektur med officiellt stöd. Den släpptes
med Debian 6.0 (Squeeze) och Debian 7.0 (Wheezy) som en <em>teknologisk
förhandstitt</em> och är den första anpassningen till något annat än Linux.
Sedan Debian 8 (Jessie) inkluderas den dock inte längre i officiella
utgåvor.</p>
</div>

<toc-add-entry name="resources">Resurser</toc-add-entry>

<p>Det finns mer information om portering (inklusive en FAQ) på
<a href="https://wiki.debian.org/Debian_GNU/kFreeBSD">Debian GNU/kFreeBSD</a>:s
wikisida.
</p>

<h3>Sändlistor</h3>
<p><a href="https://lists.debian.org/debian-bsd">Sändlista för Debian GNU/k*BSD</a>.</p>

<h3>IRC</h3>
<p><a href="irc://irc.debian.org/#debian-kbsd">IRC-kanalen #debian-kbsd </a> (på irc.debian.org).</p>

<toc-add-entry name="Development">Utveckling</toc-add-entry>

<p>Eftersom vi använder Glibc är porteringsproblem väldigt enkla och för det mesta
handlar det bara om att kopiera ett testfall för <q>k*bsd*-gnu</q> från ett annat Glibc-baserat
system (såsom GNU eller GNU/Linux). Se
<a href="https://salsa.debian.org/bsd-team/web/raw/master/porting/PORTING">porteringsdokumentet</a>
för detaljer.</p>

<p>Se även <a href="https://salsa.debian.org/bsd-team/web/raw/master/TODO">TODO</a>-filen
för fler detaljer om vad som behöver göras.</p>

<toc-add-entry name="availablehw">Tillgänglig hårdvara för Debianutvecklare</toc-add-entry>

<p>lemon.debian.net (kfreebsd-amd64) finns 
tillgängliga för Debianutvecklare i samband med anpassningsarbete. Se
<a href="https://db.debian.org/machines.cgi">maskindatabasen</a> för mer
information om dessa maskiner. Generellt har du möjlighet att använda
två chroot-miljöer: uttestnings- och den instabila utgåvan.
Observera att dessa system inte administreras av DSA, så <b>skicka inga
förfrågningar till debian-admin om dem</b>. Använd istället
<email "admin@lemon.debian.net"> .</p>
