#use wml::debian::template title="Forældet dokumentation"
#use wml::debian::translation-check translation="2d40937dc178970beff469909758a1485fd4c1d2"
#include "$(ENGLISHDIR)/doc/manuals.defs"

<h1 id="historical">Historiske dokumenter</h1>

<p>De nedenfor anførte dokumenter blev enten skrevet for længe siden og er ikke
ført ajour, eller blev skrevet til tidligere udgaver af Debian og er ikke blevet
opdateret til de aktuelle udgaver.  Oplysningerne i dokumenterne er forældet,
men kan stadig være af interesse for nogle læsere.</p>

<p>De dokumener som ikke længere er relevante og ikke længere tjener noget 
formål, har fået fjernet deres referencer, men kildekoden til mange af de 
forældede dokumenter kan findes på 
<a href="https://salsa.debian.org/ddp-team/attic">DDP's loft</a>.</p>


<h2 id="user">Brugerorienteret dokumentation</h2>

<document "dselect-dokumentation til begyndere" "dselect">

<div class="centerblock">
<p>
  Denne fil dokumenterer deselect for førstegangsbrugere, og har det mål at 
  hjælpe med at foretage en vellykket installering af Debian. Der forsøges 
  ikke på at forklare alt, derfor skal du også læse hjælpe-skærmbillederne, 
  første gang du anvender dselect.
</p>
<doctable>
  <authors "Stéphane Bortzmeyer">
  <maintainer "(?)">
  <status>
  gået i stå: <a href="https://packages.debian.org/aptitude">aptitude</a> har
  erstattet dselect som Debians standardprogram til pakkehåndteringsgrænseflade.  
  </status>
  <availability>
  <inddpcvs name="dselect-beginner" formats="html txt pdf ps"  
    langs="ca da en fr it ja hr pl pt ru sk es cs de">
  </availability>
</doctable>
</div>

<hr />

<document "Brugervejledning" "users-guide">

<div class="centerblock">
<p>
Denne <q>brugervejledning</q> er en omformatteret udgave af <q>Progenys brugervejledning</q>.
Indholdet er tilpasset til Debians-standardsystem.</p>

<p>Over 300 sider med en god vejledning i at bruge Debians styresystem fra 
den grafiske brugerflade og kommandolinien.
</p>
<doctable>
  <authors "Progeny Linux Systems, Inc.">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
  I sig selv nyttig som en vejledning.  Skrevet til udgivelsen "woody", er ved
  at blive overflødig.
  </status>
  <availability>
# langs="en" isn't redundant, it adds the needed language suffix to the link  
  <inddpcvs name="users-guide" index="users-guide" langs="en" formats="html txt pdf">
  </availability>
</doctable>
</div>

<hr />

<document "Debians lærebog" "tutorial">

<div class="centerblock">
<p>
Denne håndbog er beregnet til nye Linux-brugere, som en hjælp til at lære 
Linux at kende, når systemet er blevet installeret, eller til nye Linux-brugere 
på systemer som administreres af andre.
</p>
<doctable>
  <authors "Havoc Pennington, Oliver Elphick, Ole Tetlie, James Treacy,
  Craig Sawyer, Ivan E. Moore II">
  <editors "Havoc Pennington">
  <maintainer "(?)">
  <status>
  gået i stå; ikke færdig; muligvis forældet på grund af
  <a href="user-manuals#quick-reference">Debian-reference</a>
  </status>
  <availability>
  ikke klar endnu
  <inddpcvs name="debian-tutorial" cvsname="tutorial">
  </availability>
</doctable>
</div>

<hr />

<document "Debian GNU/Linux: Vejledning i installering og anvendelse" "guide">

<div class="centerblock">
<p>
  En håndbog, rettet mod slutbrugere.
</p>
<doctable>
  <authors "John Goerzen, Ossama Othman">
  <editors "John Goerzen">
  <status>
  færdig (men til potato)
  </status>
  <availability>
  <inoldpackage "debian-guide">
  </availability>
</doctable>
</div>

<hr />

<document "Debians brugeropslagsbog" "userref">

<div class="centerblock">
<p>
  Denne håndbog giver om ikke andet en oversigt over alt hvad en bruger bør 
  vide om sit Debian GNU/Linux-system (f.eks. opsætning af X, hvordan netværket 
  sættes op, tilgang til disketter, osv.). Det er hensigten at slå bro mellem 
  Debian-vejledningen og de detaljerede håndbøger og info-sider, der følger med 
  alle pakker.</p>

  <p>Det er også hensigten at give en vis indsigt i hvordan man kombinerer 
  kommandoer, efter det genrelle Unix-princip, at der <em>altid er mere end en 
  måde at gøre det på</em>.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Jason D. Waterman, Havoc Pennington,
      Oliver Elphick, Bruce Evry, Karl-Heinz Zimmer">
  <editors "Thalia L. Hooker, Oliver Elphick">
  <maintainer "(?)">
  <status>
  gået i stå og ganske mangelfuld; muligvis forældet på grund af
  <a href="user-manuals#quick-reference">Debian-reference</a>
  </status>
  <availability>
  <inddpcvs name="user">
  </availability>
</doctable>
</div>

<hr />

<document "Debians system-administrationshåndbog" "system">

<div class="centerblock">
<p>
  Dette dokument nævnes i indledningen til fremgangsmådehåndbogen.
  Det dækker alle aspekter af systemadministrering af et Debian-system.
</p>
<doctable>
  <authors "Tapio Lehtonen">
  <maintainer "Tapio Lehtonen">
  <status>
  gået i stå; ufuldstændig; muligvis forældet på grund af 
  <a href="user-manuals#quick-reference">Debian-reference</a> 
  </status>
  <availability>
  ikke tilgængelig endnu
  <inddpcvs name="system-administrator">
  </availability>
</doctable>
</div>

<hr />

<document "Debians netværkadministrationshåndbog" "network">

<div class="centerblock">
<p>
  Denne håndbog dækker alle aspekter af netværksadministrering af et
  Debian-system.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick, Duncan C. Thomson, Ivan E. Moore II">
  <maintainer "(?)">
  <status>
  gået i stå; ufuldstændig; muligvis forældet på grund af 
  <a href="user-manuals#quick-reference">Debian-reference</a>
  </status>
  <availability>
  ikke tilgængelig endnu
  <inddpcvs name="network-administrator">
  </availability>
</doctable>
</div>

<hr />

<document "Linux-kogebog" "linuxcookbook">

<div class="centerblock">
<p>
  En opslagsbog om Debian GNU/Linux-systemet, med over 1500 <q>opskrifter</q> på 
  hvordan man bruger systemet i dagligdagen &ndash; fra arbejde med tekst, billeder 
  og lyd, til problemer med produktivitet og netværk.  Som den software bogen
  beskriver, er den udgivet under "copyleft" og kildedataene er tilgængelige.
</p>
<doctable>
  <authors "Michael Stutz">
  <status>
  udgivet, skrevet til "woody", er ved at blive overflødig
  </status>
  <availability>
  <inoldpackage "linuxcookbook">
  <p><a href="http://dsl.org/cookbook/">fra forfatteren</a>
  </availability>
</doctable>
</div>

<hr />

<document "APT HOWTO" "apt-howto">

<div class="centerblock">
<p>
  Denne håndbog er en hurtig, men komplet, informationskilde om APT-systemet 
  og dets muligheder. Der er mange oplysninger om APT's primære 
  anvendelsesområder og mange eksempler.
  </p>
<doctable>
  <authors "Gustavo Noronha Silva">
  <maintainer "Gustavo Noronha Silva">
  <status>
  færdig
  </status>
  <availability>
  <inpackage "apt-howto">
  <br>
  <inddpcvs name="apt-howto" langs="ca en fr el it ja zh-cn zh-tw ko pl pt-br ru es cs tr de uk"
	formats="html txt pdf ps" naming="locale" />
  </availability>
</doctable>
</div>


<h2 id="devel">Udviklerdokumentation</h2>

<document "Introduktion: Fremstil en Debian-pakke" "makeadeb">

<div class="centerblock">
<p>
  Introduktion til hvordan man laver en <code>.deb</code> ved hjælp af
  <strong>debmake</strong>.
</p>
<doctable>
  <authors "Jaldhar H. Vyas">
  <status>
  stoppet, overflødiggjort af <a href="devel-manuals#maint-guide">vejledning til nye udviklere</a>
  </status>
  <availability>
  <a href="https://people.debian.org/~jaldhar/">HTML online</a>
  </availability>
</doctable>
</div>

<hr />

<document "Debians håndbog til programmører" "programmers">

<div class="centerblock">
<p>
  Vejleder nye udviklere i at oprette en pakke til Debian GNU/Linux-systemet.
</p>
<doctable>
  <authors "Igor Grobman">
  <status>
  overflødiggjort af <a href="devel-manuals#maint-guide">vejledning til nye udviklere</a>
  </status>
  <availability>
  ikke færdig
  <inddpcvs name="programmer">
  </availability>
</doctable>
</div>

<hr />

<document "Debians pakningshåndbog" "packman">

<div class="centerblock">
<p>
  Denne håndbog beskriver de tekniske aspekter ved fremstillingen af
  Debians binære- og kildekodepakker. Den dokumenterer også grænsefladen
  mellem dselect og dettes adgangsscripts. Håndbogen behandler ikke
  Debian-projekets retningslinier, og den forudsætter kendskab til dpkg's
  funktionalitet fra en systemadministrators perspektiv.
</p>
<doctable>
  <authors "Ian Jackson, Klee Dienes, David A. Morris, Christian Schwarz">
  <status>
  Delene, som var de facto-retningslinier blev flettet ind i
  <a href="devel-manuals#policy">debian-policy</a>.
  </status>
  <availability>
  <inoldpackage "packaging-manual">
  </availability>
</doctable>
</div>


<h2 id="misc">Forskellig dokumentation</h2>

<document "Debian Repository HOWTO" "repo">

<div class="centerblock">
<p>
  Dette dokument forklarer hvordan Debians arkiver (repositories) fungerer, 
  hvordan man opretter dem og hvordan man føjer dem til <tt>sources.list</tt> 
  på den rette måde.
</p>
<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  klar (?)
  </status>
  <availability>
  <inddpcvs name="repository-howto" index="repository-howto"
    formats="html" langs="en fr ta de uk">
  </availability>
</doctable>
</div>
