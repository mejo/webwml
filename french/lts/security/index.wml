#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
#use wml::debian::template title="LTS — Informations de sécurité" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<toc-display/>

<toc-add-entry name="keeping-secure">Garder son système Debian LTS sécurisé</toc-add-entry>

<p>Pour obtenir les dernières informations de sécurité de Debian LTS, abonnez-vous à
la liste de diffusion <a href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a>.
</p>

<p>Pour plus d’informations sur les problèmes de sécurité dans Debian LTS, veuillez
consulter les <a href="../../security">informations de sécurité de Debian</a>.
</p>

<a class="rss_logo" href="dla">RSS</a>
<toc-add-entry name="DLAS">Annonces récentes</toc-add-entry>

<p>Ces pages web contiennent une archive condensée des annonces de sécurité qui
sont postées sur la liste de diffusion
<a href="https://lists.debian.org/debian-lts-announce/"></a>.
</p>

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/lts/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (titles only)" href="dla">
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (summaries)" href="dla-long">
:#rss#}
<p>Les dernières annonces de sécurité de Debian LTS sont aussi disponibles au
<a href="dla">format RDF</a>. Nous proposons aussi un
<a href="dla-long">second fichier</a> qui inclut le premier paragraphe de
l’annonce de sécurité correspondante, de façon à connaître le sujet de
l’annonce.
</p>

#include "$(ENGLISHDIR)/lts/security/index.include"
<p>Les anciennes annonces de sécurité sont également disponibles :
<:= get_past_sec_list(); :>

<p>Les publications de Debian ne sont pas exposées à tous les problèmes de
sécurité. Le <a href="https://security-tracker.debian.org/">système de suivi de
sécurité de Debian</a> rassemble toutes les informations de vulnérabilité de
paquets Debian et une recherche peut être faite par nom CVE ou par paquet.
</p>


<toc-add-entry name="contact">Contacts</toc-add-entry>

<p>Veuillez lire la <a href="https://wiki.debian.org/LTS/FAQ">FAQ de Debian
LTS</a> avant de nous contacter, la réponse à votre question s’y trouve
peut-être déjà !</p>

<p>Les <a href="https://wiki.debian.org/LTS/FAQ">informations de contact</a>
sont aussi dans la FAQ.
</p>
