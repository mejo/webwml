#use wml::debian::translation-check translation="1a1de9664130db6a161a271f8f155c9b0277a03a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de Linux publiée dans <a href="dla-1731">DLA-1731-1</a> a
causé une régression dans le pilote vmxnet3 (VMware virtual network adapter).
Cette mise à jour corrige cette régression, et une précédente dans
l’implémentation du système de fichiers réseau CIFS introduites dans
<a href="../2018/dla-1422">DLA-1422-1</a>. Pour rappel, le texte de l’annonce
originale suit.</p>

<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une augmentation de droits, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10741">CVE-2016-10741</a>

<p>Une situation de compétition a été découverte dans XFS qui pourrait aboutir à
un plantage (bogue). Un utilisateur local autorisé à écrire dans un volume XFS
pourrait utiliser cela pour un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5753">CVE-2017-5753</a>

<p>De nouvelles instances de code vulnérables à Spectre variante 1
(contournement de vérification de limites) ont été mitigées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13305">CVE-2017-13305</a>

<p>Une lecture excessive de mémoire a été découverte dans le type de clefs du
sous-système de clefs chiffrées. Un utilisateur local pourrait utiliser cela pour
un déni de service ou éventuellement pour lire des informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3639">CVE-2018-3639</a> (SSB)

<p>Plusieurs chercheurs ont découvert que SSB (Speculative Store Bypass), une
fonction implémentée dans beaucoup de processeurs, pourrait être utilisé pour
lire des informations sensibles d’autre contexte. En particulier, du code dans
un bac à sable logiciel pourrait lire des informations sensibles en dehors
du bac à sable. Ce problème est aussi connu comme Spectre variante 4.</p>

<p>Cette mise à jour corrige des bogues dans les mitigations de SSB pour les
processeurs AMD.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5848">CVE-2018-5848</a>

<p>Le pilote wifi wil6210 ne validait pas correctement la longueur des requêtes
d’analyse et connexion, conduisant à un dépassement possible de tampon. Dans les
systèmes utilisant ce pilote, un utilisateur local ayant la capacité
CAP_NET_ADMIN pourrait utiliser cela pour un déni de service (corruption de
mémoire ou plantage) ou éventuellement pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5953">CVE-2018-5953</a>

<p>Le sous-système swiotlb imprimait des adresses mémoire de noyau dans le
journal du système. Cela pourrait aider un attaquant local à exploiter d’autres
vulnérabilités.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12896">CVE-2018-12896</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-13053">CVE-2018-13053</a>

<p>Team OWL337 a signalé un possible dépassement d'entier dans l’implémentation
POSIX de timer. Cela pourrait avoir des impacts de sécurité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16862">CVE-2018-16862</a>

<p>Vasily Averin et Pavel Tikhomirov de l’équipe Virtuozzo Kernel ont découvert
que la fonction de gestion mémoire cleancache n’annulait pas les données
en cache des fichiers effacés. Sur des invités Xen utilisant le pilote tmem, des
utilisateurs locaux pourraient éventuellement lire des données d’autres fichiers
d’utilisateur supprimés s’ils pouvaient créer de nouveaux fichiers dans le même
volume.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16884">CVE-2018-16884</a>

<p>Un défaut a été découvert dans l’implémentation du client NFS 4.1. Le montage
de partages dans plusieurs espaces de noms réseau en même temps pourrait
conduire à une utilisation après libération. Des utilisateurs locaux pourrait
utiliser cela pour un déni de service (corruption de mémoire ou plantage) ou
éventuellement pour une augmentation de droits.</p>

<p>Cela peut être mitigé en empêchant des utilisateurs non privilégiés de créer
des espaces de noms utilisateur, ce qui existe par défaut dans Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17972">CVE-2018-17972</a>

<p>Jann Horn a signalé que les fichiers /proc/*/stack dans procfs divulguaient
des données sensibles du noyau. Ces fichiers sont maintenant seulement lisibles
par les utilisateurs ayant la capacité CAP_SYS_ADMIN (habituellement le
superutilisateur).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18281">CVE-2018-18281</a>

<p>Jann Horn a signalé une situation de compétition dans le gestionnaire de
mémoire virtuelle. Cela peut aboutir à un processus ayant brièvement accès à la
mémoire après qu’elle ait été libérée et réallouée. Un utilisateur local pourrait
éventuellement exploiter cela pour un déni de service (corruption de mémoire) ou
pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18690">CVE-2018-18690</a>

<p>Kanda Motohiro a signalé que XFS ne gérait pas correctement des écritures
xattr (attribut étendu) qui nécessitaient un changement de format de disque de
xattr. Un utilisateur ayant accès à un volume XFS pourrait utiliser cela pour un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18710">CVE-2018-18710</a>

<p>Le pilote de CD-ROM ne validait pas correctement le paramètre ioctl
CDROM_SELECT_DISC. Un utilisateur ayant accès à un tel périphérique pourrait
utiliser cela pour lire des informations sensibles du noyau ou pour provoquer un
déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19824">CVE-2018-19824</a>

<p>Hui Peng et Mathias Payer ont découvert un bogue d’utilisation de mémoire
après libération dans le pilote audio USB. Un attaquant physiquement présent
pouvant relier un périphérique USB spécialement conçu, pourrait utiliser cela
pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19985">CVE-2018-19985</a>

<p>Hui Peng et Mathias Payer ont découvert une vérification manquante de limites
dans le périphérique série USB hso. Un attaquant physiquement présent pouvant
relier un périphérique USB spécialement conçu, pourrait utiliser cela pour lire
des informations sensibles du noyau ou pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20169">CVE-2018-20169</a>

<p>Hui Peng et Mathias Payeront ont découvert des vérifications manquantes de
limites dans le noyau USB. Un attaquant physiquement présent pouvant relier un
périphérique USB spécialement conçu, pourrait utiliser cela pour provoquer un
déni de service (plantage) ou éventuellement pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20511">CVE-2018-20511</a>

<p>InfoSect a signalé une fuite d'informations dans l’implémentation de IP/DDP
AppleTalk. Un utilisateur local avec la capacité CAP_NET_ADMIN pourrait utiliser
cela pour lire des informations sensibles du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3701">CVE-2019-3701</a>

<p>Muyu Yu et Marcus Meissner ont signalé que l’implémentation de passerelle CAN
permettait de modifier la longueur de trame, conduisant classiquement à des
écritures E/S mappées en mémoire hors limites. Sur un système avec des
périphériques CAN présents, un utilisateur local avec la capacité CAP_NET_ADMIN
dans l’espace de noms réseau initial pourrait utiliser cela pour provoquer un
plantage (oops) ou d’autres impacts dépendant du matériel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3819">CVE-2019-3819</a>

<p>Une boucle infinie potentielle a été découverte dans l’interface debugfs HID
exposée sous /sys/kernel/debug/hid. Un utilisateur ayant accès à ces fichiers
pourrait utiliser cela pour un déni de service.</p>

<p>Cette interface est seulement accessible au superutilisateur par défaut, ce
qui atténue ce problème.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6974">CVE-2019-6974</a>

<p>Jann Horn a signalé un bogue d’utilisation de mémoire après libération dans
KVM. Un utilisateur local ayant accès à /dev/kvm pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7221">CVE-2019-7221</a>

<p>Jim Mattson et Felix Wilhelm ont signalé un bogue d’utilisation après
libération dans l’implémentation de VMX imbriquée dans KVM. Sur les systèmes avec
des CPU d’Intel, un utilisateur local ayant accès à /dev/kvm pourrait utiliser
cela pour provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement pour une augmentation de droits.</p>

<p>L’imbrication de VMX est désactivée par défaut, ce qui atténue ce problème.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7222">CVE-2019-7222</a>

<p>Felix Wilhelm a signalé une fuite d'informations dans KVM pour x86. Un
utilisateur local ayant accès à /dev/kvm pourrait utiliser cela pour lire des
informations sensibles du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9213">CVE-2019-9213</a>

<p>Jann Horn a signalé que des tâches privilégiées pourraient faire que des
segments de pile, incluant ceux d’autres processus, décroissent vers
l’adresse 0. Sur les systèmes sans SMAP (x86) ou PAN (ARM), cela aggrave les
autres vulnérabilités : un déréférencement de pointeur NULL pourrait être
exploité pour une augmentation de droits plutôt que pour seulement un déni de
service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.16.64-1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>


# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1731-2.data"
# $Id: $
