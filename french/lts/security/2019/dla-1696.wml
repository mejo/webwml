#use wml::debian::translation-check translation="f2c06ef0a874aeb21cd9ebce2275905d6ef34ac0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Ceph, un système de
fichiers et de stockage distribué.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14662">CVE-2018-14662</a>

<p>Il a été découvert que des utilisateurs ceph authentifiés avec seulement des
permissions de lecture pourraient usurper les clefs de chiffrement dm-crypt
utilisées dans le chiffrement de disques ceph.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16846">CVE-2018-16846</a>

<p>Il a été découvert que des utilisateurs ceph RGW authentifiés peuvent causer
un déni de service à l’encontre d’OMAP ayant des indices de compartiment.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 0.80.7-2+deb8u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ceph.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow"
href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1696.data"
# $Id: $
